# Stable Diffusion on Custom Photographs

## Description

This project showcases the implementation of the Stable Diffusion algorithm for image transformations and enhancements. With it, you can craft unique art-like visualizations based on your own photographs. The project is inspired by and based on the works of [TheLastBen](https://colab.research.google.com/github/TheLastBen/fast-stable-diffusion/blob/main/fast-DreamBooth.ipynb), [@SurPaul](https://colab.research.google.com/github/SurPaul/stable-diffusion-webui-colab/blob/main/Stable_Diffusion_WebUI.ipynb), and [Diffusers on GitHub](https://github.com/ShivamShrirao/diffusers).

## Demonstration

Below are two examples of image processing using our algorithm:

![Example 1](./.assets/exmpl_1.jpg)
![Example 2](./.assets/exmpl_2.jpg)

## Getting Started

1. Ensure you have all the necessary dependencies installed, as listed in the ipynb notebook.
2. Upload your image into the project directory.
3. Follow the instructions in the ipynb notebook to process your image.

